package com.bbva.paom.lib.r002;

import java.util.List;
import java.util.Map;

public interface PAOMR002 {

	List<Map<String, Object>> executeGetCatalogoInformation();

}
