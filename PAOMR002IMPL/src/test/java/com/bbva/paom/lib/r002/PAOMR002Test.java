package com.bbva.paom.lib.r002;

import com.bbva.elara.configuration.manager.application.ApplicationConfigurationService;
import com.bbva.elara.domain.transaction.Context;
import com.bbva.elara.domain.transaction.ThreadContext;
import javax.annotation.Resource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.Advised;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"classpath:/META-INF/spring/PAOMR002-app.xml",
		"classpath:/META-INF/spring/PAOMR002-app-test.xml",
		"classpath:/META-INF/spring/PAOMR002-arc.xml",
		"classpath:/META-INF/spring/PAOMR002-arc-test.xml" })
public class PAOMR002Test {

	private static final Logger LOGGER = LoggerFactory.getLogger(PAOMR002Test.class);

	@Resource(name = "paomR002")
	private PAOMR002 paomR002;

	@Resource(name = "applicationConfigurationService")
	private ApplicationConfigurationService applicationConfigurationService;

	@Before
	public void setUp() throws Exception {
		ThreadContext.set(new Context());
		getObjectIntrospection();
	}
	
	private Object getObjectIntrospection() throws Exception{
		Object result = this.paomR002;
		if(this.paomR002 instanceof Advised){
			Advised advised = (Advised) this.paomR002;
			result = advised.getTargetSource().getTarget();
		}
		return result;
	}
	
	@Test
	public void executeTest(){
		LOGGER.info("Executing the test...");
		paomR002.executeGetCatalogoInformation();
	}
	
}
