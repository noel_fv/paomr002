package com.bbva.paom.lib.r002.impl;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.apx.exception.db.NoResultException;

public class PAOMR002Impl extends PAOMR002Abstract {

	private static final Logger LOGGER = LoggerFactory.getLogger("PAOMR002Impl");
	private static final String ERROR_GET_BASIC_DATA = "PAOM00000001";
	private static final String TRACE_PAOMR002_IMPL = " [PAOM][PAOMR002Impl] - %s";

	@Override
	public List<Map<String, Object>> executeGetCatalogoInformation() {
		List<Map<String, Object>> mapOut = null;

		try {
			LOGGER.info(String.format(TRACE_PAOMR002_IMPL, "executeGetCatalogoInformation START"));
			mapOut = this.jdbcUtils.queryForList("PAOM.readCatalogoData");
		} catch (NoResultException e) {
			LOGGER.error(String.format(TRACE_PAOMR002_IMPL, " Error executeGetCatalogoInformation: " + e.getMessage()));
			this.addAdvice(ERROR_GET_BASIC_DATA);
			return Collections.emptyList();
		}
		LOGGER.info(String.format(TRACE_PAOMR002_IMPL, "executeGetCatalogoInformation END"));
		return mapOut;
	}
}
